#!/usr/bin/env python3
from jinja2 import Environment, FileSystemLoader, select_autoescape, contextfilter
import os
from data import PROFESSIONAL_EXPERIENCE, PERSONAL_PROJECTS, TRANSLATIONS


BASE_DIR = os.path.abspath(os.path.dirname(__file__))
TEMPLATE_DIR = os.path.join(BASE_DIR, 'src/templates')
OUTPUT_DIR = os.path.join(BASE_DIR, 'build')


@contextfilter
def trans(ctx, string):
    if ctx.get('lang', 'es') == 'en':
        return TRANSLATIONS[string]

    return string


@contextfilter
def translated(ctx, obj, field):
    lang = ctx.get('lang', 'es')

    if lang != 'es':
        return obj[f'{field}_{lang}']

    return obj[field]


def get_environment():
    env = Environment(
        loader=FileSystemLoader(TEMPLATE_DIR),
        autoescape=select_autoescape(['html', 'xml'])
    )

    env.filters['trans'] = trans
    env.filters['translated'] = translated

    return env


def resume():
    env = get_environment()
    template = env.get_template('resume.html')

    with open(os.path.join(OUTPUT_DIR, 'resume.html'), 'w') as outfile:
        outfile.write(template.render(
            lang='es',
            profesional_experience=PROFESSIONAL_EXPERIENCE,
            personal_projects=PERSONAL_PROJECTS,
        ))

    with open(os.path.join(OUTPUT_DIR, 'resume-en.html'), 'w') as outfile:
        outfile.write(template.render(
            lang='en',
            profesional_experience=PROFESSIONAL_EXPERIENCE,
            personal_projects=PERSONAL_PROJECTS,
        ))


if __name__ == '__main__':
    resume()
